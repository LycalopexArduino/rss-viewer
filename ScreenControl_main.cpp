#include "ScreenControl_main.h"



// MISC
String fixSpecialHTML(String a) {
	String fixed = a;
	fixed.replace("&quot;", "\"");
	fixed.replace("&amp;", "&");
	return fixed;
}
// End MISC


//Ethernet
EthernetClient client;
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED }; 		// Just some random mac address
IPAddress ip(192,168,1,122);								// IP address in the LAN range
char server[] = "www.reddit.com";

void eth_connect() {							
	if (Ethernet.begin(mac) == 0) {
		clearAndPrint("DHCP failed!");
		Ethernet.begin(mac, ip);	
	}
}
void eth_getPage(int amount) {
	if (client.connect(server, 80)) {
		Serial.println("Connected");
		client.println("GET /r/arduino/.rss?limit="+String(amount)+" HTTP/1.1");
		client.println("Host: www.reddit.com");
		client.println("Connection: close");
		client.println();
	}
	else {
		clearAndPrint("Could not connect!");	
	}
}
//End Ethernet


// Screen
LiquidCrystal lcd(44,45,46,47,48,49);
int current_scr = -1;
unsigned int screen_length = 40;
// End screen


//Buttons
	int prev = 26;
	int next = 27;
	
	int srl_right = 28;
	int srl_left = 29;
	
	int last_state = 0; // One input per click only
//End buttons


// Parsing
int reading_tag_name = 0;
String current_tag_name;

int page_title_found = 0;
String page_title;

int reading_content = 0;
String current_content;
int current_content_num = 0;

int parsing_done = 0;

// End parsing


const int rss_size = 10; // Change this if more needed
String content[rss_size];	
unsigned int current_scroll = 0;

// Screen Control Commands
void clearAndPrint(String text) {
	lcd.clear();
	// Library not working properly with 4 lines, gotta quickfix that!
	if(text.length()>20) {
		for(unsigned int i = 0; (i<text.length()) && (i<screen_length); i++) {
			lcd.print(text[i]);
			if(i==19) lcd.setCursor(0,1);
			if(i==39) lcd.setCursor(0,2);
			if(i==59) lcd.setCursor(0,3);
		}
	}
	else {
		lcd.print(text);	
	}
	if(current_scr>-1) {
		lcd.setCursor(0,3);
		String title_and_sep = page_title + " | ";
		String page = String(current_scr+1) + "/" + String(rss_size);
		if(title_and_sep.length()+page.length()<=20) {
			lcd.print(title_and_sep+page);
		}
		else {
			lcd.print(page);
		}
	}
}

void nextEntry() {
	if(current_scr==rss_size-1) {				// Resetting the entry to first one
		current_scr=-1;
	}
	clearAndPrint(content[++current_scr]);
	current_scroll = 0;
}
void previousEntry() {
	if(current_scr==0) {					// Sending pointer to last entry
		current_scr=rss_size;
	}
	clearAndPrint(content[--current_scr]);
	current_scroll = 0;
}


void scrollRight(String content) {
	if(content.length()>screen_length+current_scroll) {
		current_scroll++;
		clearAndPrint(content.substring(current_scroll, current_scroll+screen_length));
	}
}
void scrollLeft(String content) {
	if(current_scroll>=1) {
		current_scroll--;
		clearAndPrint(content.substring(current_scroll, current_scroll+screen_length));
	}
	
}
// END Screen Control Commands

void setup() {
	pinMode(prev, INPUT);
	pinMode(next, INPUT);
	pinMode(srl_right, INPUT);
	pinMode(srl_left, INPUT);

	lcd.begin(20, 4);
	Serial.begin(9600);
	
	eth_connect();
	eth_getPage(rss_size);
	
	clearAndPrint("Parsing RSS feed...");
	
}
void loop() {	

	if(client.available()) 				// Reading the buffer
	{
		char c = client.read();
		if(reading_content) {			// Reading "title" content
			getTitle(c);				
		}
		else if(reading_tag_name) {		// Reading tag name
			readTagName(c);
		}
		else {							// Searching for a tag start "<"
			searchForTag(c);
		}
	}
	if (!client.connected() && parsing_done==0) {	// Done reading the buffer, will do necessary tasks before interacting with user
		parsing_done = 1;
				
		clearAndPrint(page_title);					// Output title to LCD
		
		for(int i = 0; i<rss_size; i++) {			// Output log to Serial
			Serial.println(String(i+1)+": "+content[i] +" : "+String(content[i].length()));
		}
		Serial.println();
		Serial.println("disconnecting.");
		client.stop();								// Done with Ethernet
	}
	if(parsing_done) // Start looping this after we're done with parsing etc.
	{
		if(digitalRead(next)) {						// Switch to next entry
			if(last_state==0) {
				nextEntry();
			}
			last_state=1;
		}	
		else if(digitalRead(prev)) {				// Switch to previous entry

			if(last_state==0) {
				previousEntry();
			}
			last_state=1;
		}
		else if(digitalRead(srl_right)) {
			if(last_state==0) {
				scrollRight(content[current_scr]);
			}
			last_state=1;
		}
		else if(digitalRead(srl_left)) {
			if(last_state==0) {
				scrollLeft(content[current_scr]);
			}
			last_state=1;
		
		}
		else {
			last_state = 0;
			delay(50);	// Crappy button, ignore this							
		}
	}

}
void readTagName(char c) {
	if(c != '>') {					// Still reading the tag name
		current_tag_name += c;
	}
	else {							// End of tag name
		reading_tag_name = 0;		// Stop reading tag name
		if(current_tag_name == "title") {
			reading_content = 1;		// Start reading content
		}
		current_tag_name = "";		// Clear the String
	}
}
void searchForTag(char c) {
	if(c == '<') {					// Tag start found, start reading tag
		reading_tag_name = 1;
	}
}
void getTitle(char c) {
	if(c != '<') {					// Keep on reading
		current_content += c;
	}
	else {							// Content ended
		reading_content = 0;
		if(!page_title_found) {		// First title is the page title
			page_title_found = 1;
			page_title = current_content;
		}
		else {
			if(current_content!=page_title) {						// Duplicate title ignored
				current_content = fixSpecialHTML(current_content);
				content[current_content_num] = current_content;
				current_content_num++;
			}
		}
		current_content = "";
	}
}
