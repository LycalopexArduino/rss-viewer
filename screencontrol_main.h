//-------------------------------------------------------------------
#ifndef __screencontrol_main_H__
#define __screencontrol_main_H__
//-------------------------------------------------------------------
 
#include <arduino.h>
#include <LiquidCrystal.h>
#include <util.h>
#include <EthernetUdp.h>
#include <EthernetServer.h>
#include <EthernetClient.h>
#include <Ethernet.h>
#include <Dns.h>
#include <Dhcp.h>
#include <SPI.h>
#include <w5100.cpp>
#include <socket.cpp>

 
 
//-------------------------------------------------------------------
 
//-------------------------------------------------------------------
 
// Put yout declarations here
 
//-------------------------------------------------------------------
 
//===================================================================
// -> DO NOT WRITE ANYTHING BETWEEN HERE...
// 		This section is reserved for automated code generation
// 		This process tries to detect all user-created
// 		functions in main_sketch.cpp, and inject their
// 		declarations into this file.
// 		If you do not want to use this automated process,
//		simply delete the lines below, with "&MM_DECLA" text
//===================================================================
//---- DO NOT DELETE THIS LINE -- @MM_DECLA_BEG@---------------------
void getTitle(char c);
void searchForTag(char c);
void readTagName(char c);
void loop();
void setup();
void scrollLeft(String content);
void scrollRight(String content);
void previousEntry();
void nextEntry();
void clearAndPrint(String text);
void eth_getPage(int amount);
void eth_connect();
//---- DO NOT DELETE THIS LINE -- @MM_DECLA_END@---------------------
// -> ...AND HERE. This space is reserved for automated code generation!
//===================================================================
 
 
//-------------------------------------------------------------------
#endif
//-------------------------------------------------------------------
 
 
 
 
 
